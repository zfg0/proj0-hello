**Author:** Zane Globus-O'Harra

**Contact:** zfg@uoregon.edu

### Project Description

A simple program that prints a message specified by a configuration file.
